import csv
import os
import sys


def get_phone_codes_dict():
    """ Retrun dict  celluar codes from Mari El"""
    phone_codes = dict()
    phone_codes['20'] = '7902430'
    phone_codes['24'] = '7902434'
    phone_codes['25'] = '7902435'
    phone_codes['26'] = '7902436'
    phone_codes['27'] = '7902437'
    phone_codes['28'] = '7902438'
    phone_codes['29'] = '7902432'
    phone_codes['37'] = '7902433'
    phone_codes['44'] = '7902744'
    phone_codes['47'] = '7902743'
    phone_codes['61'] = '7902101'
    phone_codes['62'] = '7902107'
    phone_codes['66'] = '7902439'
    phone_codes['65'] = '7902325'
    phone_codes['67'] = '7902107'
    phone_codes['70'] = '7902465'
    phone_codes['71'] = '7902466'
    phone_codes['75'] = '7902745'
    phone_codes['76'] = '7902736'
    phone_codes['77'] = '7902739'
    phone_codes['78'] = '7902738'
    phone_codes['79'] = '7902431'
    phone_codes['90'] = '7902670'
    phone_codes['91'] = '7902671'
    phone_codes['92'] = '7902672'
    phone_codes['93'] = '7902124'
    phone_codes['94'] = '7902664'
    phone_codes['95'] = '7902735'
    phone_codes['96'] = '7902326'
    phone_codes['97'] = '7902737'
    phone_codes['98'] = '7902358'
    phone_codes['99'] = '7902329'
    phone_codes['32'] = '7927882'
    phone_codes['33'] = '7927883'
    phone_codes['54'] = '7964864'
    phone_codes['50'] = '7964860'
    phone_codes['51'] = '7964861'
    phone_codes['52'] = '7964862'
    phone_codes['35'] = '7962588'
    return phone_codes


def get_data_from_file(filepath, delimiter=";", row_phone=1):
    data_list = list()
    with open(filepath, 'r') as file_in:
        reader = csv.reader(file_in, delimiter=delimiter)
        for row in reader:
            data_list.append(str(row[row_phone]))

    return data_list


def get_full_phone(phone_str):
    phone_codes = get_phone_codes_dict()
    first_two_letters, end_part = phone_str[:2], phone_str[2:]
    try:
        full_string = phone_codes[first_two_letters] + end_part
    except KeyError:
        full_string = ''
    finally:
        return full_string


def get_clean_phones_list(data_list):
    newlist = []
    for row in data_list:
        # if len string lower than six this is broke phone string
        if len(row) < 6:
            continue

        # clean out on "-"
        phone_str = row.replace("-", '')

        if len(phone_str) == 11:
            # checking city code in phone line
            if phone_str.startswith('78362'):
                end_part = phone_str[5:]
                final_line = get_full_phone(end_part)
            # checking start number 8
            elif phone_str.startswith('8'):
                final_line = '7' + phone_str[1:]
            # here 11-lenght normal line
            else:
                final_line = phone_str

        # checking 6-length line
        elif len(phone_str) == 6:
            final_line = get_full_phone(phone_str)

        # if lenght line is 11 than add to list
        if len(final_line) == 11:
            newlist.append(final_line)

    # set for clean out on dublicates
    return set(newlist)


def main(filepath):
    list_data_from_file = get_data_from_file(filepath)
    newlist = get_clean_phones_list(list_data_from_file)
    root, ext = os.path.splitext(filepath)
    new_filepath = '{root}(modyfile){ext}'.format(root=root, ext=ext)
    with open(new_filepath, 'w') as file_out:
        for row in newlist:
            file_out.write('{row}\n'.format(row=row))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Nepravilnie argumenti! Nado pisat: python ptel.py file'
        sys.exit(1)

    filename = sys.argv[1]
    main(filename)




