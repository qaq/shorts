#!/usr/bin/python
#! _*_ coding: utf-8 _*_
import os
from email_sender import sending_mail

# host_ip = '192.168.66.14'
# если виндовс то ставим n, иначе ставим с (это для линукса)
ping_param = 'n' if os.name == 'nt' else 'c'
ping_command = 'ping -{} 1'.format(ping_param)


def host_is_online(host_ip):
    response = os.system('{ping_cmd} {ip}'.format(ping_cmd=ping_command,
    ip=host_ip))
    return response == 0

def main(host_ip):
    if not host_is_online(host_ip):
        sending_mail('host: {} is down'.format(host_ip),
                     'HOST DOWN!',
                     'lombard_ekspress@mail.ru',
                     'pawnshop',
                     'ge52@mail.ru')

if __name__ == '__main__':
    import sys
    main(sys.argv[1])
