#! _*_ coding: utf-8 _*_

import smtplib
from email.mime.text import MIMEText


def _send_mail(msg, to, mail_user, mail_pass, smtp_server='smtp.mail.ru', smtp_port=465):
    mail_server_f = lambda x, y: smtplib.SMTP_SSL(x, y)
    mail_server = mail_server_f(smtp_server, smtp_port)
    login_f = lambda user, pasw: mail_server.login(user, pasw)
    login_f(mail_user, mail_pass)
    mail_server.sendmail(mail_user, to, msg)
    mail_server.quit()


def _get_email_msg(subject, to, text_msg):
    msg = MIMEText(text_msg)
    msg['Subject'] = subject
    msg['To'] = to
    return msg.as_string()

def sending_mail(msg_subject, msg_text, mail_user, mail_pasw, to, smtp_server='smtp.mail.ru', smtp_port=465):
    msg = _get_email_msg(msg_subject, to, msg_text)
    _send_mail(msg, to, mail_user, mail_pasw, smtp_server, smtp_port)



if __name__ == '__main__':
    import sys
    tema = sys.argv[1]
    text = sys.argv[2]
    sending_mail(tema, text, 'lombard_ekspress@mail.ru', 'pawnshop', 'ge52@mail.ru')
